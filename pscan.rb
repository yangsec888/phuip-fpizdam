#--
# pscan.rb - driver program for phuip-fpizdam
#             https://github.com/yangsec888/phuip-fpizdam
#
# Usage:
#   $ ruby pscan.rb [file with php sites]
#
#
# Copyright (c) 2019 Sam (Yang) Li
#++

require "wmap"
require "parallel"

@verbose = false
@scanned_urls = Hash.new
@max_parallel = 40
Bin_path = ENV["GOPATH"] + "/bin/"
# worker process
def scan_worker(k,url)
  # looking for a valid php endpoint for the test
  file = url.gsub(":","-").gsub("/","-")
  logfile = file + ".log"
  cmd = Bin_path + "phuip-fpizdam " + url + " --logfile " + logfile
  puts "Execution command: #{cmd}"
  system(cmd)
  return url
rescue Exception => ee
  puts "Scan error: #{ee}"
  return nil
end

# Main Program
k=Wmap::WpTracker.new(:verbose => @verbose)
prh_urls = k.file_2_list(ARGV[0]).map { |url|
  if url.include?(".php")  # already exposed php end-point
    url = url.split("?")[0]
  else
    site = k.url_2_site(url)
    if k.known_wp_sites.key?(site)
      url = site + "wp-login.php" # wp login php end-point
    else
      url = site + "index.php" # default
    end
  end
}.uniq

puts "Start parallel scannings on urls: #{prh_urls}"
Parallel.map(prh_urls, :in_processes => @max_parallel) { |target|
  puts "Working on #{target} ..." if @verbose
  scan_worker(k,target)
}.dup.each do |process|
  puts "process.inspect: #{process}" if @verbose
  url=process
  unless ["",nil].include?(url)
    @scanned_urls[url]=true unless @scanned_urls.key?(url)
  end
end
k.list_2_file(@scanned_urls.keys(), "./scanned_urls.txt")

k=nil
