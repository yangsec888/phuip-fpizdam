#--
# chk_log.rb -  phuip-fpizdam log checker
#             https://github.com/yangsec888/phuip-fpizdam
#
# Usage:
#   $ ruby chk_log.rb [file with phuip-fpizdam logs]
#
#
# Copyright (c) 2019 Sam (Yang) Li
#++
require 'wmap'

@verbose = false

# log checker worker
def chk_woker(k,file_path)
  puts "Checking log file: #{file_path}" if @verbose
  pattern_1 = "base status code is 200"
  pattern_2 = "qsl"
  pattern_3 = "probably vulnerable"
  entries = Hash.new
  k.file_2_list(file_path).map do |entry|
    entries[entry] = true
  end
  puts "Entries: #{entries}" if @verbose
  if find_line(entries,pattern_1) && find_line(entries,pattern_2) && find_line(entries,pattern_3)
      puts "Manual review need: #{file_path}"
  end
end

# simple string matcher
def find_line(entries,pattern)
  puts "Finding pattern: #{pattern}" if @verbose
  entries.keys.map do |x|
    if x.include?(pattern)
      puts "Pattern found: #{pattern}" if @verbose
      return true
    end
  end
  puts "Pattern not found: #{pattern}" if @verbose
  return false
end

#### Main Program
k=Wmap::WpTracker.new
if File.exist?(ARGV[0])
  k.file_2_list(ARGV[0]).map do |y|
    puts "Processing log file: #{y}" if @verbose
    chk_woker(k,y)
  end
else
  puts "Error file not found: #{ARGV[0]}"
end
